<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="B12727">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title type="_245">A proclamation for proroguing the Parliament.</title>
    <author>England and Wales. Sovereign (1603-1625 : James I)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription B12727 of text S123959 in the <ref target="B12727-http;//estc.bl.uk">English Short Title Catalog</ref>. Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in	 a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">B12727</idno>
    <idno type="stc">STC 8374</idno>
    <idno type="stc">Interim Tract Supplement Guide 506.h.10[54]</idno>
    <idno type="proquest">ocm99892775</idno>
    <availability>
     <p>This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. Searching, reading, printing, or downloading EEBO-TCP texts is reserved for the authorized users of these project partner institutions. Permission must be granted for subsequent distribution, in print or electronically, of this text, in whole or in part. Please contact project staff at eebotcp-info(at)umich.edu for further information or permissions.</p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online text creation partnership.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 2, no. B12727)</note>
    <note>Transcribed from: (Early English Books Online ; image set 187150)</note>
    <note>Images scanned from microfilm: (Early English books; Tract supplement ; C7:1[54])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title type="_245">A proclamation for proroguing the Parliament.</title>
      <author>England and Wales. Sovereign (1603-1625 : James I)</author>
      <author>James I, King of England, 1566-1625.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.).   </extent>
     <publicationStmt>
      <publisher>By Robert Barker, printer to the Kings most excellent Maiestie.,</publisher>
      <pubPlace>Imprinted at London :</pubPlace>
      <date>Anno Dom. 1605.</date>
     </publicationStmt>
     <notesStmt>
      <note>Dated at end: Ampthill the xxviij. day of Iuly, in the third yeere of our reigne ... .</note>
      <note>Setting 5 Nov. as the date. Cf. STC.</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <encodingDesc>
   <projectDesc>
    <p>Header created with script mrcb2eeboutf.xsl on 2016-01-28.</p>
   </projectDesc>
   <editorialDecl n="4">
    <p>Manually keyed and coded text linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
    <p>Issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
    <p>Keying and markup guidelines available at TCP web site (http://www.textcreationpartnership.org/docs/)</p>
   </editorialDecl>
  </encodingDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="lcsh">
     <term type="corporate_name">England and Wales. --  Parliament --  Law and legislation --  Early works to 1800.</term>
     <term type="geographic_name">Great Britain --  History --  James I, 1603-1625 --  Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>A proclamation for proroguing the Parliament. .</ep:title>
    <ep:author>(null)</ep:author>
    <ep:publicationYear>1605</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>273</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2014-01 </date>
    <label>SPi Global</label>Keyed and coded from ProQuest page images 
      </change>
   <change>
    <date>2014-03 </date>
    <label>Anne Simpson </label>Sampled and proofread 
      </change>
   <change>
    <date>2014-03 </date>
    <label>Anne Simpson</label>Text and markup reviewed and edited 
      </change>
   <change>
    <date>2015-03 </date>
    <label>pfs</label>Batch review (QC) and XML conversion 
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="B12727-e10010" xml:lang="eng">
  <body xml:id="B12727-e10020">
   <div type="proclamation" xml:id="B12727-e10030">
    <pb facs="1" xml:id="B12727-001-a"/>
    <head xml:id="B12727-e10040">
     <w lemma="❧" pos="sy" xml:id="B12727-001-a-0010">❧</w>
     <w lemma="by" pos="acp" xml:id="B12727-001-a-0020">By</w>
     <w lemma="the" pos="d" xml:id="B12727-001-a-0030">the</w>
     <w lemma="king" pos="n1" xml:id="B12727-001-a-0040">King</w>
     <pc unit="sentence" xml:id="B12727-001-a-0050">.</pc>
     <w lemma="a" pos="d" xml:id="B12727-001-a-0060">A</w>
     <w lemma="proclamation" pos="n1" xml:id="B12727-001-a-0070">Proclamation</w>
     <w lemma="for" pos="acp" xml:id="B12727-001-a-0080">for</w>
     <w lemma="prorogue" pos="vvg" xml:id="B12727-001-a-0090">proroguing</w>
     <w lemma="the" pos="d" xml:id="B12727-001-a-0100">the</w>
     <w lemma="parliament" pos="n1" xml:id="B12727-001-a-0110">Parliament</w>
     <pc unit="sentence" xml:id="B12727-001-a-0120">.</pc>
    </head>
    <p xml:id="B12727-e10050">
     <hi xml:id="B12727-e10060">
      <w lemma="whereas" pos="cs" rend="decorinit" xml:id="B12727-001-a-0130">WHereas</w>
      <w lemma="at" pos="acp" xml:id="B12727-001-a-0140">at</w>
      <w lemma="the" pos="d" xml:id="B12727-001-a-0150">the</w>
      <w lemma="rise" pos="n1-vg" xml:id="B12727-001-a-0160">rising</w>
      <w lemma="of" pos="acp" xml:id="B12727-001-a-0170">of</w>
      <w lemma="the" pos="d" xml:id="B12727-001-a-0180">the</w>
      <w lemma="late" pos="j" xml:id="B12727-001-a-0190">late</w>
      <w lemma="session" pos="n1" xml:id="B12727-001-a-0200">Session</w>
      <w lemma="of" pos="acp" xml:id="B12727-001-a-0210">of</w>
     </hi>
     <w lemma="our" pos="po" xml:id="B12727-001-a-0220">our</w>
     <w lemma="parliament" pos="n1" xml:id="B12727-001-a-0230">Parliament</w>
     <pc xml:id="B12727-001-a-0240">,</pc>
     <w lemma="we" pos="pns" xml:id="B12727-001-a-0250">Wee</w>
     <w lemma="prorogue" pos="vvn" xml:id="B12727-001-a-0260">prorogued</w>
     <w lemma="the" pos="d" xml:id="B12727-001-a-0270">the</w>
     <w lemma="same" pos="d" xml:id="B12727-001-a-0280">same</w>
     <w lemma="until" pos="acp" reg="until" xml:id="B12727-001-a-0290">vntill</w>
     <w lemma="the" pos="d" xml:id="B12727-001-a-0300">the</w>
     <w lemma="three" pos="ord" xml:id="B12727-001-a-0310">third</w>
     <w lemma="day" pos="n1" xml:id="B12727-001-a-0320">day</w>
     <w lemma="of" pos="acp" xml:id="B12727-001-a-0330">of</w>
     <w lemma="october" pos="nn1" xml:id="B12727-001-a-0340">October</w>
     <w lemma="now" pos="av" xml:id="B12727-001-a-0350">now</w>
     <w lemma="next" pos="ord" xml:id="B12727-001-a-0360">next</w>
     <w lemma="ensue" pos="vvg" xml:id="B12727-001-a-0370">ensuing</w>
     <pc xml:id="B12727-001-a-0380">:</pc>
     <w lemma="we" pos="pns" xml:id="B12727-001-a-0390">Wee</w>
     <w lemma="have" pos="vvb" xml:id="B12727-001-a-0400">haue</w>
     <w lemma="since" pos="acp" xml:id="B12727-001-a-0410">since</w>
     <w lemma="consider" pos="vvn" xml:id="B12727-001-a-0420">considered</w>
     <pc xml:id="B12727-001-a-0430">,</pc>
     <w lemma="that" pos="cs" xml:id="B12727-001-a-0440">that</w>
     <w lemma="the" pos="d" xml:id="B12727-001-a-0450">the</w>
     <w lemma="hold" pos="n1-vg" xml:id="B12727-001-a-0460">holding</w>
     <w lemma="of" pos="acp" xml:id="B12727-001-a-0470">of</w>
     <w lemma="it" pos="pn" xml:id="B12727-001-a-0480">it</w>
     <w lemma="so" pos="av" xml:id="B12727-001-a-0490">so</w>
     <w lemma="soon" pos="av" reg="soon" xml:id="B12727-001-a-0500">soone</w>
     <pc xml:id="B12727-001-a-0510">,</pc>
     <w lemma="be" pos="vvz" xml:id="B12727-001-a-0520">is</w>
     <w lemma="not" pos="xx" xml:id="B12727-001-a-0530">not</w>
     <w lemma="so" pos="av" xml:id="B12727-001-a-0540">so</w>
     <w lemma="convenient" pos="j" reg="convenient" xml:id="B12727-001-a-0550">conuenient</w>
     <pc xml:id="B12727-001-a-0560">,</pc>
     <w lemma="aswell" pos="av" reg="as well" xml:id="B12727-001-a-0570">aswell</w>
     <w lemma="for" pos="acp" xml:id="B12727-001-a-0580">for</w>
     <w lemma="that" pos="d" xml:id="B12727-001-a-0590">that</w>
     <w lemma="the" pos="d" xml:id="B12727-001-a-0600">the</w>
     <w lemma="ordinary" pos="j" xml:id="B12727-001-a-0610">ordinary</w>
     <w lemma="course" pos="n1" xml:id="B12727-001-a-0620">course</w>
     <w lemma="of" pos="acp" xml:id="B12727-001-a-0630">of</w>
     <w lemma="our" pos="po" xml:id="B12727-001-a-0640">our</w>
     <w lemma="subject" pos="n2-j" xml:id="B12727-001-a-0650">Subiects</w>
     <pc xml:id="B12727-001-a-0660">,</pc>
     <w lemma="resort" pos="vvg" xml:id="B12727-001-a-0670">resorting</w>
     <w lemma="to" pos="acp" xml:id="B12727-001-a-0680">to</w>
     <w lemma="the" pos="d" xml:id="B12727-001-a-0690">the</w>
     <w lemma="city" pos="n1" reg="city" xml:id="B12727-001-a-0700">Citie</w>
     <w lemma="for" pos="acp" xml:id="B12727-001-a-0710">for</w>
     <w lemma="their" pos="po" xml:id="B12727-001-a-0720">their</w>
     <w lemma="usual" pos="j" reg="usual" xml:id="B12727-001-a-0730">vsuall</w>
     <w lemma="affair" pos="n2" reg="affairs" xml:id="B12727-001-a-0740">affaires</w>
     <w lemma="at" pos="acp" xml:id="B12727-001-a-0750">at</w>
     <w lemma="the" pos="d" xml:id="B12727-001-a-0760">the</w>
     <w lemma="term" pos="n1" reg="term" xml:id="B12727-001-a-0770">Terme</w>
     <pc xml:id="B12727-001-a-0780">,</pc>
     <w lemma="be" pos="vvz" xml:id="B12727-001-a-0790">is</w>
     <w lemma="not" pos="xx" xml:id="B12727-001-a-0800">not</w>
     <w lemma="for" pos="acp" xml:id="B12727-001-a-0810">for</w>
     <w lemma="the" pos="d" xml:id="B12727-001-a-0820">the</w>
     <w lemma="most" pos="avs-d" xml:id="B12727-001-a-0830">most</w>
     <w lemma="part" pos="n1" xml:id="B12727-001-a-0840">part</w>
     <w lemma="until" pos="acp" reg="until" xml:id="B12727-001-a-0850">vntill</w>
     <w lemma="al-hallowtide" pos="j" xml:id="B12727-001-a-0860">Al-hallowtide</w>
     <pc xml:id="B12727-001-a-0870">,</pc>
     <w lemma="or" pos="cc" xml:id="B12727-001-a-0880">or</w>
     <w lemma="thereabouts" pos="av" xml:id="B12727-001-a-0890">thereabouts</w>
     <pc xml:id="B12727-001-a-0900">:</pc>
     <w lemma="as" pos="acp" xml:id="B12727-001-a-0910">As</w>
     <w lemma="also" pos="av" xml:id="B12727-001-a-0920">also</w>
     <w lemma="for" pos="acp" xml:id="B12727-001-a-0930">for</w>
     <w lemma="that" pos="cs" xml:id="B12727-001-a-0940">that</w>
     <w lemma="the" pos="d" xml:id="B12727-001-a-0950">the</w>
     <w lemma="concourse" pos="n1" xml:id="B12727-001-a-0960">concourse</w>
     <w lemma="of" pos="acp" xml:id="B12727-001-a-0970">of</w>
     <w lemma="people" pos="n1" xml:id="B12727-001-a-0980">people</w>
     <pc xml:id="B12727-001-a-0990">,</pc>
     <w lemma="which" pos="crq" xml:id="B12727-001-a-1000">which</w>
     <w lemma="follow" pos="vvz" xml:id="B12727-001-a-1010">followeth</w>
     <w lemma="the" pos="d" xml:id="B12727-001-a-1020">the</w>
     <w lemma="assembly" pos="n1" xml:id="B12727-001-a-1030">assembly</w>
     <w lemma="come" pos="vvg" reg="coming" xml:id="B12727-001-a-1040">comming</w>
     <w lemma="from" pos="acp" xml:id="B12727-001-a-1050">from</w>
     <w lemma="all" pos="d" xml:id="B12727-001-a-1060">all</w>
     <w lemma="part" pos="n2" xml:id="B12727-001-a-1070">parts</w>
     <w lemma="of" pos="acp" xml:id="B12727-001-a-1080">of</w>
     <w lemma="the" pos="d" xml:id="B12727-001-a-1090">the</w>
     <w lemma="realm" pos="n1" reg="realm" xml:id="B12727-001-a-1100">Realme</w>
     <pc xml:id="B12727-001-a-1110">,</pc>
     <w lemma="in" pos="acp" xml:id="B12727-001-a-1120">in</w>
     <w lemma="many" pos="d" xml:id="B12727-001-a-1130">many</w>
     <w lemma="whereof" pos="crq" xml:id="B12727-001-a-1140">whereof</w>
     <w lemma="there" pos="av" xml:id="B12727-001-a-1150">there</w>
     <w lemma="may" pos="vmb" xml:id="B12727-001-a-1160">may</w>
     <w lemma="yet" pos="av" xml:id="B12727-001-a-1170">yet</w>
     <w lemma="remain" pos="vvi" reg="remain" xml:id="B12727-001-a-1180">remaine</w>
     <w lemma="some" pos="d" xml:id="B12727-001-a-1190">some</w>
     <w lemma="dregs" pos="n2" xml:id="B12727-001-a-1200">dregs</w>
     <w lemma="of" pos="acp" xml:id="B12727-001-a-1210">of</w>
     <w lemma="the" pos="d" xml:id="B12727-001-a-1220">the</w>
     <w lemma="late" pos="j" xml:id="B12727-001-a-1230">late</w>
     <w lemma="contagion" pos="n1" xml:id="B12727-001-a-1240">Contagion</w>
     <pc xml:id="B12727-001-a-1250">,</pc>
     <w lemma="may" pos="vmb" xml:id="B12727-001-a-1260">may</w>
     <w lemma="be" pos="vvi" xml:id="B12727-001-a-1270">be</w>
     <w lemma="a" pos="d" xml:id="B12727-001-a-1280">an</w>
     <w lemma="occasion" pos="n1" xml:id="B12727-001-a-1290">occasion</w>
     <w lemma="to" pos="acp" xml:id="B12727-001-a-1300">to</w>
     <w lemma="revive" pos="vvi" reg="revive" xml:id="B12727-001-a-1310">reuiue</w>
     <w lemma="it" pos="pn" xml:id="B12727-001-a-1320">it</w>
     <w lemma="in" pos="acp" xml:id="B12727-001-a-1330">in</w>
     <w lemma="that" pos="d" xml:id="B12727-001-a-1340">that</w>
     <w lemma="place" pos="n1" xml:id="B12727-001-a-1350">place</w>
     <pc xml:id="B12727-001-a-1360">,</pc>
     <w lemma="where" pos="crq" xml:id="B12727-001-a-1370">where</w>
     <w lemma="our" pos="po" xml:id="B12727-001-a-1380">our</w>
     <w lemma="most" pos="avs-d" xml:id="B12727-001-a-1390">most</w>
     <w lemma="abode" pos="n1" xml:id="B12727-001-a-1400">abode</w>
     <w lemma="be" pos="vvz" xml:id="B12727-001-a-1410">is</w>
     <pc unit="sentence" xml:id="B12727-001-a-1420">.</pc>
     <w lemma="and" pos="cc" xml:id="B12727-001-a-1430">And</w>
     <w lemma="therefore" pos="av" xml:id="B12727-001-a-1440">therefore</w>
     <w lemma="we" pos="pns" xml:id="B12727-001-a-1450">Wee</w>
     <w lemma="have" pos="vvb" xml:id="B12727-001-a-1460">haue</w>
     <w lemma="think" pos="vvn" xml:id="B12727-001-a-1470">thought</w>
     <w lemma="it" pos="pn" xml:id="B12727-001-a-1480">it</w>
     <w lemma="fit" pos="j" xml:id="B12727-001-a-1490">fit</w>
     <w lemma="to" pos="acp" xml:id="B12727-001-a-1500">to</w>
     <w lemma="prorogue" pos="vvi" xml:id="B12727-001-a-1510">prorogue</w>
     <w lemma="it" pos="pn" xml:id="B12727-001-a-1520">it</w>
     <w lemma="further" pos="avc-j" xml:id="B12727-001-a-1530">further</w>
     <w lemma="for" pos="acp" xml:id="B12727-001-a-1540">for</w>
     <w lemma="one" pos="crd" xml:id="B12727-001-a-1550">one</w>
     <w lemma="month" pos="ng2" xml:id="B12727-001-a-1560">moneths</w>
     <w lemma="space" pos="n1" xml:id="B12727-001-a-1570">space</w>
     <pc xml:id="B12727-001-a-1580">,</pc>
     <w lemma="which" pos="crq" xml:id="B12727-001-a-1590">which</w>
     <w lemma="will" pos="vmb" xml:id="B12727-001-a-1600">will</w>
     <w lemma="fall" pos="vvi" xml:id="B12727-001-a-1610">fall</w>
     <w lemma="out" pos="av" xml:id="B12727-001-a-1620">out</w>
     <w lemma="upon" pos="acp" xml:id="B12727-001-a-1630">vpon</w>
     <w lemma="tuesday" pos="nn1" xml:id="B12727-001-a-1640">Tuesday</w>
     <w lemma="the" pos="d" xml:id="B12727-001-a-1650">the</w>
     <w lemma="five" pos="ord" xml:id="B12727-001-a-1660">fifth</w>
     <w lemma="day" pos="n1" xml:id="B12727-001-a-1670">day</w>
     <w lemma="of" pos="acp" xml:id="B12727-001-a-1680">of</w>
     <w lemma="november" pos="nn1" reg="november" xml:id="B12727-001-a-1690">Nouember</w>
     <w lemma="next" pos="ord" xml:id="B12727-001-a-1700">next</w>
     <pc xml:id="B12727-001-a-1710">,</pc>
     <w lemma="at" pos="acp" xml:id="B12727-001-a-1720">at</w>
     <w lemma="which" pos="crq" xml:id="B12727-001-a-1730">which</w>
     <w lemma="day" pos="n1" xml:id="B12727-001-a-1740">day</w>
     <w lemma="our" pos="po" xml:id="B12727-001-a-1750">our</w>
     <w lemma="purpose" pos="n1" xml:id="B12727-001-a-1760">purpose</w>
     <w lemma="be" pos="vvz" xml:id="B12727-001-a-1770">is</w>
     <pc xml:id="B12727-001-a-1780">(</pc>
     <w lemma="god" pos="nn1" xml:id="B12727-001-a-1790">God</w>
     <w lemma="will" pos="vvg" xml:id="B12727-001-a-1800">willing</w>
     <pc xml:id="B12727-001-a-1810">)</pc>
     <w lemma="to" pos="acp" xml:id="B12727-001-a-1820">to</w>
     <w lemma="hold" pos="vvi" xml:id="B12727-001-a-1830">hold</w>
     <w lemma="the" pos="d" xml:id="B12727-001-a-1840">the</w>
     <w lemma="same" pos="d" xml:id="B12727-001-a-1850">same</w>
     <pc unit="sentence" xml:id="B12727-001-a-1860">.</pc>
     <w lemma="and" pos="cc" xml:id="B12727-001-a-1870">And</w>
     <w lemma="do" pos="vvb" reg="do" xml:id="B12727-001-a-1880">doe</w>
     <w lemma="hereby" pos="av" xml:id="B12727-001-a-1890">hereby</w>
     <w lemma="give" pos="vvi" reg="give" xml:id="B12727-001-a-1900">giue</w>
     <w lemma="notice" pos="n1" xml:id="B12727-001-a-1910">notice</w>
     <w lemma="to" pos="acp" xml:id="B12727-001-a-1920">to</w>
     <w lemma="all" pos="d" xml:id="B12727-001-a-1930">all</w>
     <pc xml:id="B12727-001-a-1940">,</pc>
     <w lemma="who" pos="crq" xml:id="B12727-001-a-1950">whom</w>
     <w lemma="it" pos="pn" xml:id="B12727-001-a-1960">it</w>
     <w lemma="concern" pos="vvz" xml:id="B12727-001-a-1970">concerneth</w>
     <pc xml:id="B12727-001-a-1980">,</pc>
     <w lemma="that" pos="cs" xml:id="B12727-001-a-1990">that</w>
     <w lemma="they" pos="pns" xml:id="B12727-001-a-2000">they</w>
     <w lemma="may" pos="vmb" xml:id="B12727-001-a-2010">may</w>
     <w lemma="frame" pos="vvi" xml:id="B12727-001-a-2020">frame</w>
     <w lemma="their" pos="po" xml:id="B12727-001-a-2030">their</w>
     <w lemma="affair" pos="n2" reg="affairs" xml:id="B12727-001-a-2040">affaires</w>
     <w lemma="accord" pos="av-vg" xml:id="B12727-001-a-2050">accordingly</w>
     <pc xml:id="B12727-001-a-2060">,</pc>
     <w lemma="and" pos="cc" xml:id="B12727-001-a-2070">and</w>
     <w lemma="attend" pos="vvi" xml:id="B12727-001-a-2080">attend</w>
     <w lemma="at" pos="acp" xml:id="B12727-001-a-2090">at</w>
     <w lemma="the" pos="d" xml:id="B12727-001-a-2100">the</w>
     <w lemma="say" pos="j-vn" reg="said" xml:id="B12727-001-a-2110">saide</w>
     <w lemma="five" pos="ord" xml:id="B12727-001-a-2120">fifth</w>
     <w lemma="day" pos="n1" xml:id="B12727-001-a-2130">day</w>
     <w lemma="of" pos="acp" xml:id="B12727-001-a-2140">of</w>
     <w lemma="november" pos="nn1" reg="november" xml:id="B12727-001-a-2150">Nouember</w>
     <w lemma="to" pos="acp" xml:id="B12727-001-a-2160">to</w>
     <w lemma="that" pos="d" xml:id="B12727-001-a-2170">that</w>
     <w lemma="service" pos="n1" reg="service" xml:id="B12727-001-a-2180">Seruice</w>
     <pc unit="sentence" xml:id="B12727-001-a-2190">.</pc>
    </p>
    <closer xml:id="B12727-e10070">
     <dateline xml:id="B12727-e10080">
      <hi xml:id="B12727-e10090">
       <w lemma="give" pos="vvn" reg="given" xml:id="B12727-001-a-2200">Giuen</w>
       <w lemma="at" pos="acp" xml:id="B12727-001-a-2210">at</w>
       <w lemma="our" pos="po" xml:id="B12727-001-a-2220">our</w>
       <w lemma="honour" pos="n1" xml:id="B12727-001-a-2230">Honour</w>
       <w lemma="of" pos="acp" xml:id="B12727-001-a-2240">of</w>
       <w lemma="ampthill" pos="nn1" xml:id="B12727-001-a-2250">Ampthill</w>
       <date xml:id="B12727-e10100">
        <w lemma="the" pos="d" xml:id="B12727-001-a-2260">the</w>
        <w lemma="xxviij" pos="crd" xml:id="B12727-001-a-2270">xxviij</w>
        <pc xml:id="B12727-001-a-2280">.</pc>
        <w lemma="day" pos="n1" xml:id="B12727-001-a-2290">day</w>
        <w lemma="of" pos="acp" xml:id="B12727-001-a-2300">of</w>
        <w lemma="ju" pos="nn1" reg="july" xml:id="B12727-001-a-2310">Iuly</w>
        <pc xml:id="B12727-001-a-2320">,</pc>
        <w lemma="in" pos="acp" xml:id="B12727-001-a-2330">in</w>
        <w lemma="the" pos="d" xml:id="B12727-001-a-2340">the</w>
        <w lemma="three" pos="ord" xml:id="B12727-001-a-2350">third</w>
        <w lemma="year" pos="n1" reg="year" xml:id="B12727-001-a-2360">yeere</w>
        <w lemma="of" pos="acp" xml:id="B12727-001-a-2370">of</w>
        <w lemma="our" pos="po" xml:id="B12727-001-a-2380">our</w>
        <w lemma="reign" pos="vvi" reg="reign" xml:id="B12727-001-a-2390">Reigne</w>
        <w lemma="of" pos="acp" xml:id="B12727-001-a-2400">of</w>
        <hi xml:id="B12727-e10110">
         <w lemma="great" pos="j" xml:id="B12727-001-a-2410">Great</w>
         <w lemma="britain" pos="nn1" reg="britain" xml:id="B12727-001-a-2420">Britaine</w>
         <pc xml:id="B12727-001-a-2430">,</pc>
         <w lemma="france" pos="nn1" xml:id="B12727-001-a-2440">France</w>
        </hi>
        <w lemma="and" pos="cc" xml:id="B12727-001-a-2450">and</w>
        <w lemma="ireland" pos="nn1" rend="hi" xml:id="B12727-001-a-2460">Ireland</w>
        <pc unit="sentence" xml:id="B12727-001-a-2470">.</pc>
       </date>
      </hi>
     </dateline>
    </closer>
    <closer xml:id="B12727-e10120">
     <w lemma="god" pos="nn1" xml:id="B12727-001-a-2480">God</w>
     <w lemma="save" pos="acp" reg="save" xml:id="B12727-001-a-2490">saue</w>
     <w lemma="the" pos="d" xml:id="B12727-001-a-2500">the</w>
     <w lemma="king" pos="n1" xml:id="B12727-001-a-2510">King</w>
     <pc unit="sentence" xml:id="B12727-001-a-2520">.</pc>
    </closer>
   </div>
  </body>
  <back xml:id="B12727-e10130">
   <div type="colophon" xml:id="B12727-e10140">
    <p xml:id="B12727-e10150">
     <pc xml:id="B12727-001-a-2530">¶</pc>
     <w lemma="imprint" pos="vvn" xml:id="B12727-001-a-2540">Imprinted</w>
     <w lemma="at" pos="acp" xml:id="B12727-001-a-2550">at</w>
     <w lemma="london" pos="nn1" xml:id="B12727-001-a-2560">London</w>
     <w lemma="by" pos="acp" xml:id="B12727-001-a-2570">by</w>
     <w lemma="robert" pos="nn1" xml:id="B12727-001-a-2580">Robert</w>
     <w lemma="barker" pos="nn1" xml:id="B12727-001-a-2590">Barker</w>
     <pc xml:id="B12727-001-a-2600">,</pc>
     <w lemma="printer" pos="nn1" xml:id="B12727-001-a-2610">Printer</w>
     <w lemma="to" pos="acp" xml:id="B12727-001-a-2620">to</w>
     <w lemma="the" pos="d" xml:id="B12727-001-a-2630">the</w>
     <w lemma="king" pos="n2" xml:id="B12727-001-a-2640">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="B12727-001-a-2650">most</w>
     <w lemma="excellent" pos="j" xml:id="B12727-001-a-2660">Excellent</w>
     <w lemma="majesty" pos="n1" reg="majesty" xml:id="B12727-001-a-2670">Maiestie</w>
     <pc unit="sentence" xml:id="B12727-001-a-2680">.</pc>
    </p>
    <p xml:id="B12727-e10160">
     <w lemma="n/a" pos="fla" xml:id="B12727-001-a-2690">ANNO</w>
     <w lemma="n/a" pos="fla" xml:id="B12727-001-a-2700">DOM</w>
     <pc unit="sentence" xml:id="B12727-001-a-2710">.</pc>
     <w lemma="1605." pos="crd" xml:id="B12727-001-a-2720">1605.</w>
     <pc unit="sentence" xml:id="B12727-001-a-2730"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
